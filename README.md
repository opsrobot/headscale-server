# Install a Headscale Controller Server using Docker Compose

Headscale is an open source, self-hosted implementation of the Tailscale control server.

Find more about it at https://github.com/juanfont/headscale

Tailscale is a modern VPN built on top of Wireguard. It works like an overlay network between the computers of your networks - using NAT traversal.

Find more about it at https://tailscale.com/

This configuration requires docker and docker-composed intalled on a Ubuntu Server 20.04 instance without firewalld or ufw.

1. Enable IPv4 forwarding:
```bash
sudo echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf

sudo sysctl -p
```
Reboot the instance (recommended)
```bash
sudo reboot now
```


2.  Create the headscale folder and go there:

```bash
mkdir -p /opt/headscale && cd /opt/headscale
```

3. Copy the sample configuration file

```bash
mkdir -p ./config
touch ./config/db.sqlite
curl https://raw.githubusercontent.com/juanfont/headscale/main/config-example.yaml -o ./config/config.yaml
```
4. Edit the config file and set to the Headscale Controller Server URL to your domain/IP:

```bash
nano /opt/headscale/config/config.yaml
```

Look for the url value and modify as needed:
```bash
server_url: https://yourdomain.tld:443

listen_addr: 0.0.0.0:443

ip_prefixes:
  - fd7a:115c:a1e0::/48 # optional IPv6
  - 100.64.0.0/10  # set to the prefered IPv4 network

For extra privacy (limiting relaying to the embeded DERP server), comment out the following block:

# List of externally available DERP maps encoded in JSON
  #urls:
  #  - https://controlplane.tailscale.com/derpmap/default

Complete the Letsencrypt configuration to enable HTTPS:

# Email to register with ACME provider
acme_email: "your@email.com"

# Domain name to request a TLS certificate for:
tls_letsencrypt_hostname: "yourdomain.tld"


```
5. Create a file /opt/headscale/docker-compose.yml and paste the following content iside it:

```
version: '3.5'
services:
  headscale-server:
    image: headscale/headscale:latest
    volumes:
      - ./config:/etc/headscale/
      - ./data:/var/lib/headscale
    ports:
      - 80:80
      - 443:443
      - 3478:3478
    command: headscale serve
    restart: unless-stopped
```

6. Run the following command to start the server:
```bash
docker-compose up -d
```
7. To execute commands from the headscale CLI use the following syntax:

```bash
cd /opt/headscale/

docker-compose exec headscale headscale <command>
```
Or to make it easier to use just run the following commands to add an alias to your terminal:
```bash
echo "alias headscale='docker-compose -f /opt/headscale/docker-compose.yml exec headscale-server headscale'" >> ~/.bashrc

source ~/.bashrc
```
Now you can execute the headscale cli commands like this:
```bash
headscale nodes list
```

8. The Login URL to use with tailscale commands will be:
```bash
https://yourdomain.tld
```

# Basic Hadscale/Tailscale management

## Managing Users

### Create user:

In the Headscale Controller Server:
1. Create a user id: 
```bash
headscale users create <USER_ID>
```
2. Generate auth keys: 
```bash
headscale --user <USER_ID> preauthkeys create --reusable --expiration 24h
```
Note: Expiration is optional and not recommended

### Install Tailscale on your client computer:

1. Linux (all)
```bash
curl -fsSL https://tailscale.com/install.sh | sh
```

Or (Ubuntu/Debian/Mint)
```bash
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/focal.gpg | sudo apt-key add -
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/focal.list | sudo tee /etc/apt/sources.list.d/tailscale.list
```
```bash
sudo apt-get update
sudo apt-get install tailscale
```

2. Windows
https://github.com/juanfont/headscale/blob/main/docs/windows-client.md

3. MacOS

    Visit the url of your headscale server for specific instruction:

    http://yourdomain.tld/apple

### Configure login at the Client Computer:
Log in the comuter to the taiscale network:
```bash
tailscale up --login-server <YOUR_HEADSCALE_URL> --authkey <YOUR_AUTH_KEY> --accept-routes
```

## Managing routes:

### Connect Node and advertise routes:

1. Enable IPv4 forwarding in the client computer (Ubuntu/Debian example) :
```bash
sudo echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf

sudo sysctl -p
```
Reboot the instance (recommended)
```bash
sudo reboot now
```

2. In the node you want to use to advertise routes (gateway) do:
```bash
tailscale up --advertise-routes=10.0.0.0/24 --login-server=<YOUR_HEADSCALE_URL> --accept-routes --authkey <USER_KEY>
```
### Enable routes in the Headscale Controller Server:

1. List the currently advertised routes: 
```bash
headscale routes list

   Example:
   ID | Machine           | Prefix      | Advertised | Enabled | Primary
   1  | node-machine      | 10.0.0.0/24 | true       | true    | true 
```
2. Enable the necessary route: 
```bash
headscale routes enable -r <ROUTE_ID>
```
### Delete user:

In the Headscale Controller Server:
```bash
headscale users destroy <USER_ID>
```